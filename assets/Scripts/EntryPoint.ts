import { _decorator, Component } from 'cc';
import { GameManager } from './GameManager';
const { ccclass, property } = _decorator;

@ccclass('EntryPoint')
export class EntryPoint extends Component {
    @property(GameManager)
    GameManager: GameManager;

    start() {
        this.GameManager.StartGame();
    }
}

