import { _decorator, Component, log, Node, Prefab, Vec2, EventTarget } from 'cc';
import { Cell, CellEventNames } from './Cell';
import { PlayerSymbol } from './PlayerSymbol';
const { ccclass, property } = _decorator;

@ccclass('GridService')
export class GridService extends Component {
    @property(Node)
    GridRoot: Node;
    @property(Cell)
    Cells: Cell[] = [];

    public Event: EventTarget = new EventTarget();
    public Rows: number;
    public Columns: number;

    public _grid: Map<number, Cell> = new Map();

    protected start(): void {
        this.GridRoot.active = false;

        this.Rows = 3;
        this.Columns = 3;

        for (let i = 0; i < this.Rows; i++) {
            for (let j = 0; j < this.Columns; j++) {
                const index = i * this.Columns + j;
                let item = this.Cells[index];
                let vector = new Vec2(i, j);
                item.node.name = 'Cell x: ' + vector.x + ' y: ' + vector.y;
                item.SetPosition(vector);
                item.SetSymbol(PlayerSymbol.None);
                item.Event.on(CellEventNames.ClickEvent, this.OnCellClicked, this);
                this._grid.set(index, item);
            }
        }
    }

    public Init():void{
        this.GridRoot.active = true;
    }

    public Dispose():void{
        this.GridRoot.active = false;
    }

    public Reset(){
        for(var cell of this.Cells){
            cell.SetSymbol(PlayerSymbol.None);
        }
    }

    public GetCell(position: Vec2): Cell | undefined{
        const index = position.x * this.Columns + position.y;
        if(this._grid.has(index) === false){
            return undefined;
        }

        return this._grid.get(index);
    }

    private OnCellClicked(cell: Cell){
        this.Event.emit(GridServiceEventNames.ClickCellEvent, cell);
    }
}

export enum GridServiceEventNames{
    ClickCellEvent
}

