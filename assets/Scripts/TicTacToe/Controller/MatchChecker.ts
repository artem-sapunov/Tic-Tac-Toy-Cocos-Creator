import { Vec2, log } from "cc";
import { GridService } from "../GridService";
import { IMatch } from "./IMatch";
import { IMatchChecker } from "./IMatchChecker";
import { PlayerSymbol } from "../PlayerSymbol";
import { Match } from "./Match";

export class MatchChecker implements IMatchChecker {

    private _gridService: GridService;
    private _sequence: Vec2[];

    private _winCount: number = 3;

    constructor(gridService: GridService){
        this._gridService = gridService;
    }

    public CheckMatch(gridService: GridService): IMatch | undefined {
        this._gridService = gridService;
        for (let i = 0; i < this._gridService.Rows; i++) {
            for (let j = 0; j < this._gridService.Columns; j++) {
                const position = new Vec2(i, j);
                const cell = gridService.GetCell(position);
                if(cell === undefined || cell.Symbol === PlayerSymbol.None){
                    continue;
                }

                const symbol = cell.Symbol;

                if(this.CalculateLinearMatch(position, symbol, new Vec2(0, 1))){
                    return new Match(symbol, [...this._sequence])
                }
                if(this.CalculateLinearMatch(position, symbol, new Vec2(1, 0))){
                    return new Match(symbol, [...this._sequence])
                }
                if(this.CalculateLinearMatch(position, symbol, new Vec2(1, 1))){
                    return new Match(symbol, [...this._sequence])
                }
                if(this.CalculateLinearMatch(position, symbol, new Vec2(-1, 1))){
                    return new Match(symbol, [...this._sequence])
                }
            }
        }

        return undefined;
    }

    private CalculateLinearMatch(checkPosition: Vec2, symbol: PlayerSymbol, direction: Vec2): boolean{
        this._sequence = new Array<Vec2>();

        this._sequence.push(checkPosition);
        //log('start check: ' + checkPosition);

        let position = new Vec2(checkPosition.x, checkPosition.y);

        while(true){
            position = position.add(direction);
            //log(position);

            if(position.x >= this._gridService.Columns || position.y >= this._gridService.Rows){
                break;
            }

            if(this.IsMatch(position, symbol) === false){
                break;
            }

            this._sequence.push(position);

            if(this._sequence.length === this._winCount){
                return true;
            }
        }

        return false;
    }

    private IsMatch(position: Vec2, symbol: PlayerSymbol): boolean{
        const cell = this._gridService.GetCell(position);

        if(cell !== undefined){
            return cell.Symbol === symbol;
        }

        return false;
    }
}

