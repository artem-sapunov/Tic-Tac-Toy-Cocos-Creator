import { EventTarget, Vec2 } from 'cc';
import { PlayerSymbol } from '../PlayerSymbol';
import { Cell } from '../Cell';

export interface IPlayer{
    Event: EventTarget;
    PlayerSymbol: PlayerSymbol;
    Dispose(): void;
    Subscribe(): void;
    Unsubscribe(): void;
}

export enum PlayerEventName{
    OnPlayerMoved
}

export class PlayerMovedEvent{
    public Player: IPlayer;
    public Cell: Cell;

    constructor(player: IPlayer, cell: Cell){
        this.Player = player;
        this.Cell = cell;
    }
}

