import { EventTarget } from 'cc';

export interface ITicTacToyController {
    Event: EventTarget;
    StartGame(): void;
    Reset(): void;
    Undo(): void;
    Dispose(): void;
}

export enum ITicTacToyControllerEvents{

}
