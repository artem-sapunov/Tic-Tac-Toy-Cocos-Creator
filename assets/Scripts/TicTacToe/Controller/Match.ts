import { Vec2, math } from "cc";
import { PlayerSymbol } from "../PlayerSymbol";
import { IMatch } from "./IMatch";

export class Match implements IMatch {
    PlayerSymbol: PlayerSymbol;
    Positions: Vec2[];

    constructor(playerSymbol: PlayerSymbol, positions: Vec2[]){
        this.PlayerSymbol = playerSymbol;
        this.Positions = positions;
    }
}

