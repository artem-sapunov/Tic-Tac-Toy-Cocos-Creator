import { __private, EventTarget, log } from "cc";
import { PlayerSymbol } from "../PlayerSymbol";
import { IPlayer, PlayerEventName, PlayerMovedEvent } from "./IPlayer";
import { GridService, GridServiceEventNames } from "../GridService";
import { Cell } from "../Cell";

export class Player implements IPlayer {
    Event: EventTarget = new EventTarget();
    PlayerSymbol: PlayerSymbol;

    private _gridService: GridService;

    constructor(gridService: GridService, symbol: PlayerSymbol){
        this._gridService = gridService;
        this.PlayerSymbol = symbol;
    }

    Dispose(): void {
        this.Unsubscribe();
    }

    Subscribe(){
        this._gridService.Event.on(GridServiceEventNames.ClickCellEvent, this.OnCellClicked, this);
    }

    Unsubscribe(){
        this._gridService.Event.off(GridServiceEventNames.ClickCellEvent, this.OnCellClicked, this);
    }

    OnCellClicked(cell: Cell):void{
        log('OnCellClicked');
        this.Event.emit(PlayerEventName.OnPlayerMoved, new PlayerMovedEvent(this, cell));
    }
}

