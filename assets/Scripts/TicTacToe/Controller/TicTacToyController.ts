import { __private, _decorator, Component, Node, Vec2, EventTarget, log } from 'cc';
import { GridService } from '../GridService';
import { ITicTacToyController } from './ITicTacToyController';
import { IPlayer, PlayerEventName, PlayerMovedEvent } from './IPlayer';
import { PlayerSymbol } from '../PlayerSymbol';
import { IMatch } from './IMatch';
import { IMatchChecker } from './IMatchChecker';

export class TicTacToyController implements ITicTacToyController {

    public Event: EventTarget = new EventTarget();

    private _gridService: GridService;
    private _matchCheker: IMatchChecker;
    private _moves: Array<Vec2> = [];
    private _players: Array<IPlayer> = [];
    private _currentPlayer: IPlayer = undefined;
    private _listener;

    constructor(gridService: GridService, player1: IPlayer, player2: IPlayer, matchCheker: IMatchChecker){
        this._gridService = gridService;
        this._matchCheker = matchCheker;
        this._players.push(player1);
        this._players.push(player2);
    }

    StartGame(): void {
        this.NextPlayer();
    }

    Reset(): void {
        this.UnsubscribeFromCurrentPlayer();
        this._gridService.Reset();
        this.NextPlayer();
    }

    Undo(): void {
    }

    Dispose(): void{
        for(var player of this._players){
            player.Dispose();
        }
    }

    private NextPlayer(){
        this.UnsubscribeFromCurrentPlayer();
        const playerIndex = this._moves.length % this._players.length;
        this._currentPlayer = this._players[playerIndex];
        this._currentPlayer.Subscribe();
        this._listener = this._currentPlayer.Event.on(PlayerEventName.OnPlayerMoved, this.OnPlayerMoved, this);
        log('NextPlayer ' + this._currentPlayer.PlayerSymbol);
    }

    private UnsubscribeFromCurrentPlayer(){
        if(this._currentPlayer == undefined){
            return;
        }

        this._currentPlayer.Unsubscribe();
        this._currentPlayer.Event.off(PlayerEventName.OnPlayerMoved);
        this._currentPlayer = undefined;
    }

    private OnPlayerMoved(event: PlayerMovedEvent){
        const cell = this._gridService.GetCell(event.Cell.Position);

        if(cell.Symbol !== PlayerSymbol.None){
            return;
        }

        cell.SetSymbol(event.Player.PlayerSymbol);
        this._moves.push(event.Cell.Position);

        const match = this._matchCheker.CheckMatch(this._gridService);

        if(match !== undefined){
            this.UnsubscribeFromCurrentPlayer();
            this.Reset();
            log('Player win: ' + match.PlayerSymbol);
            return;
        }

        //try to find match;
        this.NextPlayer();
    }
}

