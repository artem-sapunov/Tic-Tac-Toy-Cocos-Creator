import { GridService } from "../GridService";
import { IMatch } from "./IMatch";

export interface IMatchChecker {
    CheckMatch(gridService: GridService): IMatch | undefined;
}

