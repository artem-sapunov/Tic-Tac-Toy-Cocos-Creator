import { Vec2 } from "cc";
import { PlayerSymbol } from "../PlayerSymbol";

export interface IMatch {
    PlayerSymbol: PlayerSymbol;
    Positions: Array<Vec2>;
}

