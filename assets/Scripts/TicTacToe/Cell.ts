import { _decorator, Component, Node, EventTarget, EventMouse, Vec2, Sprite, SpringJoint2D, SpriteFrame, log, RichText } from 'cc';
import { PlayerSymbol } from './PlayerSymbol';
const { ccclass, property } = _decorator;

@ccclass('Cell')
export class Cell extends Component {
    @property(Sprite)
    SymbolSprite: Sprite;
    @property(SpriteFrame)
    XSprite: SpriteFrame;
    @property(SpriteFrame)
    OSprite: SpriteFrame;
    @property(RichText)
    Text: RichText;

    public Event: EventTarget = new EventTarget();
    public Position: Vec2;
    public Symbol: PlayerSymbol;

    start() {
        this.node.on(Node.EventType.MOUSE_DOWN, this.StartGameClickHandler, this);
    }

    public SetPosition(position: Vec2){
        this.Position = position;
        //this.Text.string = 'x: ' + position.x + ' y: ' + position.y;
    }

    public SetSymbol(symbol: PlayerSymbol){
        this.Symbol = symbol;

        if(symbol === PlayerSymbol.O){
            this.SymbolSprite.spriteFrame = this.OSprite;
        }
        else if(symbol === PlayerSymbol.X){
            this.SymbolSprite.spriteFrame = this.XSprite;
        }
        else if(symbol === PlayerSymbol.None){
            this.SymbolSprite.spriteFrame = null;
        }
    }

    private StartGameClickHandler (event: EventMouse): void{
        this.Event.emit(CellEventNames.ClickEvent, this);
    }
}

export enum CellEventNames{
    ClickEvent,
}

