import { _decorator, Component, Node } from 'cc';
import { IGameStateMachine } from './States/IGameStateMachine';
import { GameStateMachine } from './States/GameStateMachine';
import { UIService } from './UI/UIService';
import { LobbyState } from './States/LobbyState';
import { GameplayState } from './States/GameplayState';
import { GridService } from './TicTacToe/GridService';
const { ccclass, property } = _decorator;

@ccclass('GameManager')
export class GameManager extends Component {
    @property(GridService)
    GridService: GridService;
    @property(UIService)
    UIService: UIService;

    private _gameStateMachine: IGameStateMachine;

    start() {
        this._gameStateMachine = new GameStateMachine(this);
        this._gameStateMachine.Add(new LobbyState());
        this._gameStateMachine.Add(new GameplayState());
    }

    StartGame(){
        this._gameStateMachine.Enter(LobbyState);
    }
}

