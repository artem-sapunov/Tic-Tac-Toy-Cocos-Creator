import { BaseExitState } from "./BaseExitState";
import { IPayloadState } from "./IPlayloadState";

abstract class BasePayloadState<T> extends BaseExitState implements IPayloadState<T>{
    abstract Enter(payload: T): void;
}

export {BasePayloadState};