import { GameManager } from "../GameManager";
import { IGameStateMachine } from "./IGameStateMachine";

interface IExitState{
    Init(stateMachine: IGameStateMachine, gameManager: GameManager): void;
    Exit(): void;
}

export type {IExitState};