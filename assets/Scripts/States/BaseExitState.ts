import { GameManager } from "../GameManager";
import { IExitState } from "./IExitState";
import { IGameStateMachine } from "./IGameStateMachine";

abstract class BaseExitState implements IExitState{
    protected _gameStateMachine: IGameStateMachine;
    protected _gameManager: GameManager;

    Init(stateMachine: IGameStateMachine, gameManager: GameManager): void {
        this._gameStateMachine = stateMachine;
        this._gameManager = gameManager;
    }
    abstract Exit(): void;
}

export {BaseExitState};
