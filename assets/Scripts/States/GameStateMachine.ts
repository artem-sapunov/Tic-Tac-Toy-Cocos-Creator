import { GameManager } from "../GameManager";
import { IExitState } from "./IExitState";
import { IGameStateMachine } from "./IGameStateMachine";
import { IPayloadState } from "./IPlayloadState";
import { IState } from "./IState";

class GameStateMachine implements IGameStateMachine {
    private _gameManager: GameManager;
    private _activeState: IExitState | null = null;
    private _states: Map<new () => IExitState, IExitState> = new Map();

    constructor(gameManager: GameManager){
        this._gameManager = gameManager;
    }

    public Add(state: IExitState): void {
        this._states.set(state.constructor as new () => IExitState, state);
        state.Init(this, this._gameManager);
    }

    public Enter<TState extends IState>(stateType: new () => TState): void {
        const state = this.ChangeState<TState>(stateType);
        state.Enter();
    }

    public EnterPayload<TState extends IPayloadState<TPayload>, TPayload>(stateType: new () => TState, payload: TPayload): void {
        const state = this.ChangeState<TState>(stateType);
        state.Enter(payload);
    }

    private ChangeState<TState extends IExitState>(stateType: new () => IExitState): TState {
        if (this._activeState !== null) {
            this._activeState.Exit();
        }

        const newState = this._states.get(stateType) as TState;
        this._activeState = newState;

        return newState;
    }
}

export {GameStateMachine};

