import { IExitState } from "./IExitState";

interface IState extends IExitState{
    Enter();
}

export type {IState};