import { BaseExitState } from "./BaseExitState";
import { IState } from "./IState";

abstract class BaseState extends BaseExitState implements IState{
    abstract Enter(): void;
}

export {BaseState};
