import { IExitState } from "./IExitState";

interface IPayloadState<T> extends IExitState{
    Enter(payload: T);
}

export type {IPayloadState}