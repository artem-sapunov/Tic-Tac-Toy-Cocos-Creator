import { _decorator, Button, Component, EventHandler, Node } from 'cc';
import { BaseState } from './BaseState';
import { LobbyView } from '../UI/GameModeMenuView';
import { WindowType } from '../UI/WindowTypes';
import { GameplayState } from './GameplayState';
const { ccclass, property } = _decorator;

class LobbyState extends BaseState {
    private _lobbyView: LobbyView;

    Enter(): void {
        this._lobbyView = this._gameManager.UIService.Show<LobbyView>(WindowType.GameModeMenu);
        this._lobbyView.StartButton.node.on(Button.EventType.CLICK, this.StartGameClickHandler, this);
    }
    Exit(): void {
        this._gameManager.UIService.Hide(WindowType.GameModeMenu);
    }

    StartGameClickHandler (button: Button) {
        this._gameStateMachine.Enter(GameplayState);
    }
}

export {LobbyState};

