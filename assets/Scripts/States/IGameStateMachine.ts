import { IExitState } from "./IExitState";
import { IPayloadState } from "./IPlayloadState";
import { IState } from "./IState";

interface IGameStateMachine{
    Add(state: IExitState): void;
    Enter<TState extends IState>(stateType: new () => TState): void;
    Enter<TState extends IPayloadState<TPayload>, TPayload>(stateType: new () => TState, payload: TPayload): void;
}

export type {IGameStateMachine};

