import { _decorator, Component, Node } from 'cc';
import { BaseState } from './BaseState';
import { TicTacToyController } from '../TicTacToe/Controller/TicTacToyController';
import { Player } from '../TicTacToe/Controller/Player';
import { PlayerSymbol } from '../TicTacToe/PlayerSymbol';
import { ITicTacToyController } from '../TicTacToe/Controller/ITicTacToyController';
import { MatchChecker } from '../TicTacToe/Controller/MatchChecker';
const { ccclass, property } = _decorator;

class GameplayState extends BaseState {
    private _ticTacToyController: ITicTacToyController;

    Enter() {
        this._gameManager.GridService.Init();

        const player1 = new Player(this._gameManager.GridService, PlayerSymbol.X);
        const player2 = new Player(this._gameManager.GridService, PlayerSymbol.O);
        const matchCheker = new MatchChecker(this._gameManager.GridService);
        
        this._ticTacToyController = new TicTacToyController(this._gameManager.GridService, player1, player2, matchCheker);
        this._ticTacToyController.StartGame();
    }
    Exit(): void {
        this._ticTacToyController.Dispose();
        this._gameManager.GridService.Dispose();
    }
}

export {GameplayState};

