import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Event')
export class Event extends Component {
    start() {

    }

    update(deltaTime: number) {
        
    }
}

