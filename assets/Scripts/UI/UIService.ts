import { _decorator, Component, Enum, instantiate, Node, Prefab, TypeScript } from 'cc';
import { UIWindow } from './UIWindow';
import { WindowType } from './WindowTypes';
const { ccclass, property } = _decorator;

@ccclass('WindowPrefabData')
class WindowPrefabData
{
    @property({type: Enum(WindowType)})
    WindowType: number = WindowType.None;
    @property(Prefab)
    WindowPrefab: Prefab | null = null;
}

@ccclass('ActiveWindowData')
class ActiveWindowData
{
    WindowType: number;
    Window: Node;
}

@ccclass('UIService')
export class UIService extends Component {
    @property(Node)
    WindowsRoot: Node;
    @property(WindowPrefabData)
    Windows: WindowPrefabData[] = [];

    private _activeWindows: ActiveWindowData[] = [];

    public Show<T extends UIWindow>(windowType: number): T | undefined{

        const windowData = this.Windows.find(x => x.WindowType === windowType);
        const instance = instantiate(windowData.WindowPrefab);
        instance.setParent(this.WindowsRoot);
        const windowInstance = instance.getComponent(UIWindow);
        const activeWindowData = new ActiveWindowData();
        activeWindowData.Window = instance;
        activeWindowData.WindowType = windowData.WindowType;
        this._activeWindows.push(activeWindowData);
        return windowInstance as T;
    }

    public Hide(windowType: number){
        const windowData = this._activeWindows.find(x => x.WindowType === windowType);
        if(windowData !== undefined){
            windowData.Window.destroy();
        }
    }
}

