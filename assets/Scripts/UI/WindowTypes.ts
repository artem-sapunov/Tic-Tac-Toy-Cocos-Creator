import { Enum } from "cc";

const WindowType = Enum({

    None: 0,
    GameModeMenu: 1
});

export { WindowType };