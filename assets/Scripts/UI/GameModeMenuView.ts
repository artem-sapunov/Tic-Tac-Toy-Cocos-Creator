import { _decorator, Button, Component, Node } from 'cc';
import { UIWindow } from './UIWindow';
const { ccclass, property } = _decorator;

@ccclass('LobbyView')
export class LobbyView extends UIWindow {
    @property(Button)
    StartButton:Button;
}

